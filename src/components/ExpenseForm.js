import React from 'react';
import styled from 'styled-components';

const Form = styled.form`
    display: flex;
    flex-direction: column;
    background-color: #fff;
    height: auto;
    max-width: 1200px;
    margin: 50px 0;
    padding: 20px 40px;
`;
const InputContainer = styled.div`
    display: flex;
    flex: 1;
    flex-direction: column;
    margin: 20px;
`;
const Label = styled.label`
    margin-bottom: 5px;
    color: #a5a5a5;
`;
const ContainerWrapper = styled.div`
    display: flex;
`;
const Button = styled.button`
    width: 200px;
    margin: 0 auto;
    border-radius: 10px;
    padding: 5px 10px;
    border: 0;
`;

const ExpenseForm = ({
    charge,
    amount,
    handleCharge,
    handleAmount,
    handleSubmit,
    edit,
}) => {
    return (
        <Form onSubmit={handleSubmit}>
            <div>
                <ContainerWrapper>
                    <InputContainer>
                        <Label htmlFor='charge'>Charge</Label>
                        <input
                            type='type'
                            placeholder='e.g. rent'
                            value={charge}
                            onChange={handleCharge}
                        />
                    </InputContainer>
                    <InputContainer>
                        <Label htmlFor='charge'>Amount</Label>
                        <input
                            type='number'
                            placeholder='e.g. 100'
                            value={amount}
                            onChange={handleAmount}
                        />
                    </InputContainer>
                </ContainerWrapper>
            </div>
            <Button type='submit'>{edit ? 'Edit' : 'Submit'}</Button>
        </Form>
    );
};

export default ExpenseForm;
