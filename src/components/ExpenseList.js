import React from 'react';
import Item from './ExpenseItem';
import styled from 'styled-components';

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
`;
const Button = styled.button`
    width: 200px;
    margin: 0 auto;
    border-radius: 10px;
    padding: 5px 10px;
    border: 0;
`;

const ExpenseList = ({ expenses, setExpenses, handleDelete, handleEdit }) => {
    const handleClearExpenses = () => {
        setExpenses([]);
    };
    return (
        <>
            <Wrapper>
                <ul>
                    {expenses.map((expense) => (
                        <Item
                            key={expense.id}
                            expense={expense}
                            handleDelete={handleDelete}
                            handleEdit={handleEdit}
                        />
                    ))}
                </ul>
                {expenses.length > 0 && (
                    <Button onClick={handleClearExpenses}>Clear exenses</Button>
                )}
            </Wrapper>
        </>
    );
};

export default ExpenseList;
