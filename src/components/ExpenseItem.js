import React from 'react';
import { AiFillDelete } from 'react-icons/ai';
import { MdEdit } from 'react-icons/md';
import styled, { keyframes } from 'styled-components';

const animationItem = keyframes`
        0%{
            top: -15px;
            opacity: 0
        }
        100%{
            top: 0;
            opacity: 1;
        }
`;

const List = styled.li`
    list-style: none;
    border: 1px solid #000;
    margin: 20px 0;
    padding: 5px 10px;
    background-color: #fff;
    border-radius: 10px;
    border: 0;
    animation: ${animationItem} 0.5s ease-in;
`;
const ListContent = styled.div`
    display: flex;
    justify-content: space-between;
`;
const Span = styled.span`
    margin-right: 5px;
`;
const Item = ({ expense, handleDelete, handleEdit }) => {
    const { id, charge, amount } = expense;

    return (
        <List>
            <ListContent>
                <span>{charge}</span>
                <span>${amount}</span>
                <div>
                    <Span onClick={() => handleEdit(id)}>
                        <MdEdit />
                    </Span>
                    <Span onClick={() => handleDelete(id)}>
                        <AiFillDelete />
                    </Span>
                </div>
            </ListContent>
        </List>
    );
};

export default Item;
