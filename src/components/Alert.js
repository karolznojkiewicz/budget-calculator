import React from 'react';
import styled from 'styled-components';

const AlertContent = styled.div`
    padding: 0.75rem 1.25rem;
    color: #fff;
    text-align: center;
    text-transform: capitalize;
    margin: 2rem auto 0 auto;
    border-radius: 2px;
    background-color: ${({ type }) => (type === 'success' ? 'green' : 'red')};
`;

const Alert = ({ type, text }) => {
    return <AlertContent type={type}>{text}</AlertContent>;
};

export default Alert;
