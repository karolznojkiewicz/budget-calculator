import styled from 'styled-components';
import Alert from './components/Alert';
import ExpenseForm from './components/ExpenseForm';
import ExpenseList from './components/ExpenseList';
import { v4 as uuidv4 } from 'uuid';
import { useEffect, useState } from 'react';

const budget = [];

const AppContainer = styled.div`
    height: 100vh;
    width: 100vw;
    background-color: #aaa;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`;
const Main = styled.div`
    margin: 0 15px;
`;
const Heading = styled.h1`
    color: #000;
`;

function App() {
    const initialState = JSON.parse(localStorage.getItem('banan')) || [];
    const [expenses, setExpenses] = useState(initialState);
    const [charge, setCharge] = useState('');
    const [amount, setAmount] = useState('');
    const [alert, setAlert] = useState({ show: false });
    const [edit, setEdit] = useState(false);
    const [id, setId] = useState(0);
    const handleCharge = (e) => {
        setCharge(e.target.value);
    };
    const handleAmount = (e) => {
        setAmount(e.target.value);
    };
    const handleAlert = ({ type, text }) => {
        setAlert({ show: true, type, text });
        setTimeout(() => {
            setAlert({ show: false });
        }, 3000);
    };
    const handleSubmit = (e) => {
        e.preventDefault();
        if (charge !== '' && amount > 0) {
            if (edit) {
                let tempExpenses = expenses.map((item) => {
                    return item.id === id ? { ...item, charge, amount } : item;
                });
                setExpenses(tempExpenses);
                handleAlert({ type: 'success', text: 'Item edited' });
                setEdit(false);
            } else {
                const singleExponse = { id: uuidv4(), charge, amount };
                setExpenses([...expenses, singleExponse]);
                handleAlert({ type: 'success', text: 'Item added' });
            }
            setCharge('');
            setAmount('');
        } else {
            handleAlert({
                type: 'danger',
                text: `Value can't be empty and value has to be bigger than zero`,
            });
        }
    };
    const handleDelete = (id) => {
        let tempExpenses = expenses.filter((expense) => expense.id !== id);
        setExpenses(tempExpenses);
        handleAlert({
            type: 'danger',
            text: `Item delete`,
        });
    };
    const handleEdit = (id) => {
        let expense = expenses.find((item) => item.id === id);
        let { charge, amount } = expense;
        setCharge(charge);
        setAmount(amount);
        setEdit(true);
        setId(id);
    };

    useEffect(() => {
        localStorage.setItem('banan', JSON.stringify(expenses));
    }, [expenses]);
    return (
        <>
            <AppContainer>
                {alert.show && <Alert type={alert.type} text={alert.text} />}

                <Heading>Budget Calculator</Heading>
                <Main>
                    <ExpenseForm
                        charge={charge}
                        amount={amount}
                        handleCharge={handleCharge}
                        handleAmount={handleAmount}
                        handleSubmit={handleSubmit}
                        edit={edit}
                    />
                    <ExpenseList
                        expenses={expenses}
                        setExpenses={setExpenses}
                        handleDelete={handleDelete}
                        handleEdit={handleEdit}
                    />
                </Main>
                <Heading>
                    Total Spending : $
                    {expenses.reduce((acc, curr) => {
                        return (acc += parseInt(curr.amount));
                    }, 0)}
                </Heading>
            </AppContainer>
        </>
    );
}

export default App;
